<?xml version="1.0" encoding="UTF-8"?>
<tileset name="main" tilewidth="64" tileheight="64" tilecount="18" columns="0">
 <tile id="0">
  <properties>
   <property name="wall" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/wall.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="start" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/start.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="end" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/end.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="color" type="int" value="1"/>
   <property name="door" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/door0.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="color" type="int" value="1"/>
   <property name="pickup" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/pickup0.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="color" type="int" value="2"/>
   <property name="door" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/door1.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="color" type="int" value="2"/>
   <property name="pickup" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/pickup1.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="color" type="int" value="3"/>
   <property name="door" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/door2.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="color" type="int" value="3"/>
   <property name="pickup" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/pickup2.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="fall" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/fall.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="jump" type="int" value="1"/>
   <property name="up" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/up.png"/>
 </tile>
 <tile id="12">
  <properties>
   <property name="down" type="int" value="1"/>
   <property name="jump" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/down.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="jump" type="int" value="1"/>
   <property name="left" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/left.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="jump" type="int" value="1"/>
   <property name="right" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/right.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="jump2" type="int" value="1"/>
   <property name="up" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/up2.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="down" type="int" value="1"/>
   <property name="jump2" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/down2.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="jump2" type="int" value="1"/>
   <property name="left" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/left2.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="jump2" type="int" value="1"/>
   <property name="right" type="int" value="1"/>
  </properties>
  <image width="64" height="64" source="../sprites/right2.png"/>
 </tile>
</tileset>
