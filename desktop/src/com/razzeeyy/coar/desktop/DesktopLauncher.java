package com.razzeeyy.coar.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.razzeeyy.coar.CoarGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "KOLO: A Color Maze Puzzle";
		config.width = 480;
		config.height = 800;
		new LwjglApplication(new CoarGame(), config);
	}
}
