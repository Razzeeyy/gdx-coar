package com.razzeeyy.coar.constants;

/**
 * Created by razze on 19.05.2017.
 */

public abstract class Events {
    public static final int PLAYER_FINISHED = 1;
    public static final int PLAYER_PICKUP = 2;
}
