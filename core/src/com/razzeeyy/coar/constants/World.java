package com.razzeeyy.coar.constants;

/**
 * Created by razze on 02.05.2017.
 */

public abstract class World {
    public static final float WIDTH = 15;
    public static final float HEIGHT = 15;
    public static final float UNIT = 64;

    public static final float PLAYER_SPEED = 5;
    public static final float CAMERA_SPEED = 4;
}
