package com.razzeeyy.coar.constants;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by razze on 23.05.2017.
 */

public abstract class Colors {
    public static final Color background = new Color(250 / 255f, 235 / 255f, 215 / 255f, 1);
}
