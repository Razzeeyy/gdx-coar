package com.razzeeyy.coar.constants;

/**
 * Created by razze on 28.09.2017.
 */

public abstract class Settings {
    public static final String SOUND = "sound";
    public static final String MUSIC = "music";
    public static final String CONTROL = "control";
}
