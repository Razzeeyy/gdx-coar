package com.razzeeyy.coar.constants;

import com.razzeeyy.coar.descriptors.PackDescriptor;

/**
 * Created by razze on 18.05.2017.
 */

public abstract class Levels {
    public static final PackDescriptor[] packs = new PackDescriptor[6];

    static {
        int i = 0;

        final PackDescriptor introPack = new PackDescriptor("SIMPLE", "images/packs/welcome.png"); //tutorials
        packs[i++] = introPack; //0
        introPack.addLevel("1", "levels/level00.tmx");
        introPack.addLevel("2", "levels/level01.tmx");
        introPack.addLevel("3", "levels/level02.tmx");
        introPack.addLevel("4", "levels/level03.tmx");
        introPack.addLevel("5", "levels/level04.tmx");
        introPack.addLevel("6", "levels/level05.tmx");
        introPack.addLevel("7", "levels/level06.tmx");
        introPack.addLevel("8", "levels/level07.tmx");
        introPack.addLevel("9", "levels/level08.tmx");
        introPack.addLevel("10", "levels/level09.tmx");

        final PackDescriptor complexPack = new PackDescriptor("COMPLEX", "images/packs/complex.png", true); //harder maps with no special features
        packs[i++] = complexPack; //1
        complexPack.addLevel("1", "levels/level00.tmx");
        complexPack.addLevel("2", "levels/level00.tmx");
        complexPack.addLevel("3", "levels/level00.tmx");
        complexPack.addLevel("4", "levels/level00.tmx");
        complexPack.addLevel("5", "levels/level00.tmx");
        complexPack.addLevel("6", "levels/level00.tmx");
        complexPack.addLevel("7", "levels/level00.tmx");
        complexPack.addLevel("8", "levels/level00.tmx");
        complexPack.addLevel("9", "levels/level00.tmx");
        complexPack.addLevel("10", "levels/level00.tmx");

        final PackDescriptor colorsPack = new PackDescriptor("COLORS", "images/packs/colors.png"); //tutorials to color changing
        packs[i++] = colorsPack; //2
        colorsPack.addLevel("1", "levels/level20.tmx");
        colorsPack.addLevel("2", "levels/level21.tmx");
        colorsPack.addLevel("3", "levels/level22.tmx");
        colorsPack.addLevel("4", "levels/level23.tmx");
        colorsPack.addLevel("5", "levels/level24.tmx");
        colorsPack.addLevel("6", "levels/level25.tmx");
        colorsPack.addLevel("7", "levels/level26.tmx");
        colorsPack.addLevel("8", "levels/level27.tmx");
        colorsPack.addLevel("9", "levels/level28.tmx");
        colorsPack.addLevel("10", "levels/level29.tmx");

        final PackDescriptor vividPack = new PackDescriptor("VIVID", "images/packs/placeholder.png", true); //harder maps with color changing
        packs[i++] = vividPack; //3
        vividPack.addLevel("1", "levels/level00.tmx");
        vividPack.addLevel("2", "levels/level00.tmx");
        vividPack.addLevel("3", "levels/level00.tmx");
        vividPack.addLevel("4", "levels/level00.tmx");
        vividPack.addLevel("5", "levels/level00.tmx");
        vividPack.addLevel("6", "levels/level00.tmx");
        vividPack.addLevel("7", "levels/level00.tmx");
        vividPack.addLevel("8", "levels/level00.tmx");
        vividPack.addLevel("9", "levels/level00.tmx");
        vividPack.addLevel("10", "levels/level00.tmx");

        final PackDescriptor doubtPack = new PackDescriptor("SPACE", "images/packs/space.png"); //introduction to "fall" tiles (teleport to spawn without losing color) and "jump" tiles (jumps over 1 tile)
        packs[i++] = doubtPack; //4
        doubtPack.addLevel("1", "levels/level40.tmx");
        doubtPack.addLevel("2", "levels/level41.tmx");
        doubtPack.addLevel("3", "levels/level42.tmx");
        doubtPack.addLevel("4", "levels/level43.tmx");
        doubtPack.addLevel("5", "levels/level44.tmx");
        doubtPack.addLevel("6", "levels/level45.tmx");
        doubtPack.addLevel("7", "levels/level46.tmx");
        doubtPack.addLevel("8", "levels/level47.tmx");
        doubtPack.addLevel("9", "levels/level48.tmx");
        doubtPack.addLevel("10", "levels/level49.tmx");

        final PackDescriptor boxedPack = new PackDescriptor("BOXED", "images/packs/boxed.png"); //maps made of several boxes snapped together (feature a variety of special features)
        packs[i++] = boxedPack; //5
        boxedPack.addLevel("1", "levels/level50.tmx");
        boxedPack.addLevel("2", "levels/level51.tmx");
        boxedPack.addLevel("3", "levels/level52.tmx");
        boxedPack.addLevel("4", "levels/level53.tmx");
        boxedPack.addLevel("5", "levels/level54.tmx");
        boxedPack.addLevel("6", "levels/level55.tmx");
        boxedPack.addLevel("7", "levels/level56.tmx");
        boxedPack.addLevel("8", "levels/level57.tmx");
        boxedPack.addLevel("9", "levels/level58.tmx");
        boxedPack.addLevel("10", "levels/level59.tmx");

    }
}
