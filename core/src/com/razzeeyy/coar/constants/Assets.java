package com.razzeeyy.coar.constants;

/**
 * Created by razze on 02.05.2017.
 */

public abstract class Assets {
    public static final String SPRITE_PLAYER = "sprites/player.png";
    public static final String SPRITE_FLOOR = "sprites/floor.png";
    public static final String SPRITE_DOOR1 = "sprites/door0.png";
    public static final String SPRITE_DOOR2 = "sprites/door1.png";
    public static final String SPRITE_DOOR3 = "sprites/door2.png";

    public static final String IMAGE_BUTTON = "images/button.png";
    public static final String IMAGE_RESTART = "images/restart.png";
    public static final String IMAGE_PIXEL = "images/pixel.png";
    public static final String IMAGE_SETTINGS = "images/settings.png";
    public static final String IMAGE_MUSIC = "images/sound.png";
    public static final String IMAGE_SOUND = "images/volume.png";
    public static final String IMAGE_CONTROL = "images/control.png";

    public static final String FONT = "font.fnt";

    public static final String SOUND_PICKUP_1 = "sounds/pickup1.wav";
    public static final String SOUND_PICKUP_2 = "sounds/pickup2.wav";
    public static final String SOUND_PICKUP_3 = "sounds/pickup3.wav";
    public static final String SOUND_JUMP = "sounds/jump.wav";
    public static final String SOUND_BUMP = "sounds/bump.wav";
    public static final String SOUND_FALL = "sounds/teleport.wav";
    public static final String SOUND_STEP = "sounds/step.wav";
    public static final String SOUND_SPAWN = "sounds/spawn.wav";
    public static final String SOUND_FINISH = "sounds/finish.wav";

    public static final String SOUND_BUTTON = "sounds/button.wav";

    public static final String PARTICLE_TRAIL = "particles/trail.p";
    public static final String PARTICLE_PICKUP = "particles/pickup.p";

    public static final String MUSIC_MENU = "music/music1.ogg";
    public static final String MUSIC_GAME1 = "music/music2.ogg";
    public static final String MUSIC_GAME2 = "music/music3.ogg";
}
