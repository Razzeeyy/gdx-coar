package com.razzeeyy.coar.constants;

/**
 * Created by razze on 03.05.2017.
 */

@SuppressWarnings("PointlessBooleanExpression")
public abstract class Debug {
    public static final boolean debug = false;

    public static final boolean unlockAllLevels = debug && true;
    public static final boolean showHiddenLevels = debug && false;

    public static final boolean guiOutline = debug && false;
    public static final boolean playerOutline = debug && true;

    public static final boolean noDesktop = debug && false;

    public static final boolean allowMusicSkip = debug && true;

    public static final boolean externalAssets = debug && false;

    public static final boolean developmentMark = debug && true;

    public static final boolean development = debug && false;
}
