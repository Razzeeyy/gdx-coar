package com.razzeeyy.coar.actors;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.razzeeyy.coar.constants.Assets;
import com.razzeeyy.coar.constants.Events;

/**
 * Created by razze on 08.06.2017.
 */

public final class ParticlesActor extends Actor implements Telegraph {
    private final MessageDispatcher messages;
    private final ParticleEffect playerTrail;
    private final PlayerActor playerActor;
    private final ParticleEffectPool pickupParticles;
    private final Array<ParticleEffectPool.PooledEffect> particleEffects;

    public ParticlesActor(AssetManager assets, MessageDispatcher messages, PlayerActor playerActor) {
        this.messages = messages;
        this.playerActor = playerActor;
        playerTrail = assets.get(Assets.PARTICLE_TRAIL, ParticleEffect.class);

        pickupParticles = new ParticleEffectPool(assets.get(Assets.PARTICLE_PICKUP, ParticleEffect.class), 5, 20);

        particleEffects = new Array<ParticleEffectPool.PooledEffect>();
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);

        if (stage != null) {
            messages.addListener(this, Events.PLAYER_PICKUP);
        } else {
            messages.removeListener(this, Events.PLAYER_PICKUP);

            for (int i = particleEffects.size - 1; i >= 0; i--) { //remove particles to prevent memory leaks
                ParticleEffectPool.PooledEffect effect = particleEffects.get(i);
                effect.reset();
                effect.free();
                particleEffects.removeIndex(i);
            }
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (playerTrail.isComplete()) {
            playerTrail.reset();
            playerTrail.start();
        }

        playerTrail.setPosition(playerActor.getX() + 0.5f, playerActor.getY() + 0.5f);

        playerTrail.update(delta);

        for (int i = particleEffects.size - 1; i >= 0; i--) {
            ParticleEffectPool.PooledEffect effect = particleEffects.get(i);

            effect.update(delta);

            if (effect.isComplete()) {
                effect.reset();
                effect.free();
                particleEffects.removeIndex(i);
            }
        }

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        playerTrail.draw(batch);

        for (ParticleEffectPool.PooledEffect effect : particleEffects) {
            effect.draw(batch);
        }
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        if (msg.message == Events.PLAYER_PICKUP) {
            ParticleEffectPool.PooledEffect pickupEffect = pickupParticles.obtain();
            pickupEffect.setPosition(playerActor.getX() + 0.5f, playerActor.getY() + 0.5f);
            pickupEffect.start();
            particleEffects.add(pickupEffect);
            return true;
        }
        return false;
    }
}
