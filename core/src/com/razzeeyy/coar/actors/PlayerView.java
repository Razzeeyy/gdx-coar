package com.razzeeyy.coar.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.razzeeyy.coar.actions.PlayerFinishedAction;
import com.razzeeyy.coar.actions.PlayerPickupAction;
import com.razzeeyy.coar.actions.SoundAction;
import com.razzeeyy.coar.constants.Assets;
import com.razzeeyy.coar.constants.Settings;

/**
 * Created by razze on 03.05.2017.
 */

public final class PlayerView extends Image implements PlayerModel.PlayerModelListener {
    private final float speed;
    private final float stepTime;

    private final MessageDispatcher messages;
    private final Preferences preferences;

    private final Array<Sound> pickupSounds;
    private final Sound collideSound;
    private final Sound jumpSound;
    private final Sound fallSound;
    private final Sound stepSound;
    private final Sound spawnSound;
    private final Sound finishSound;

    private final float PICKUP_MIXDOWN_VOLUME = 1f;
    private final float COLLIDE_MIXDOWN_VOLUME = 0.25f;
    private final float JUMP_MIXDOWN_VOLUME = 0.55f;
    private final float FALL_MIXDOWN_VOLUME = 0.55f;
    private final float STEP_MIXDOWN_VOLUME = 0.6f;
    private final float SPAWN_MIXDOWN_VOLUME = 1f;
    private final float FINISH_MIXDOWN_VOLUME = 1f;

    private final int STEP_MIXDOWN_THROTTLE = 2;
    private int steps = 0;


    public PlayerView(AssetManager assets, MessageDispatcher messages, Preferences preferences, float speed) {
        super(assets.get(Assets.SPRITE_PLAYER, Texture.class));

        setSize(1, 1);

        this.speed = speed;
        stepTime = 1 / speed;

        this.messages = messages;
        this.preferences = preferences;

        pickupSounds = new Array<Sound>();
        pickupSounds.add(assets.get(Assets.SOUND_PICKUP_1, Sound.class));
        pickupSounds.add(assets.get(Assets.SOUND_PICKUP_2, Sound.class));
        pickupSounds.add(assets.get(Assets.SOUND_PICKUP_3, Sound.class));

        collideSound = assets.get(Assets.SOUND_BUMP, Sound.class);
        jumpSound = assets.get(Assets.SOUND_JUMP, Sound.class);
        fallSound = assets.get(Assets.SOUND_FALL, Sound.class);
        stepSound = assets.get(Assets.SOUND_STEP, Sound.class);
        spawnSound = assets.get(Assets.SOUND_SPAWN, Sound.class);
        finishSound = assets.get(Assets.SOUND_FINISH, Sound.class);
    }

    @Override
    public void act(float delta) {
        Array<Action> actions = getActions();
        Stage stage = getStage();

        if (actions.size > 0) { //execute actions sequentially (in opposite to parallel which is default for any other actors)
            if (stage != null && stage.getActionsRequestRendering())
                Gdx.graphics.requestRendering();
            final int i = 0;
            Action action = actions.get(i);
            if (action.act(delta) && i < actions.size) {
                Action current = actions.get(i);
                int actionIndex = current == action ? i : actions.indexOf(action, true);
                if (actionIndex != -1) {
                    actions.removeIndex(actionIndex);
                    action.setActor(null);
                }
            }
        }
    }

    @Override
    public void onSpawned(int x, int y) {
        if(isSoundEnabled()) {
            final SoundAction soundAction = Actions.action(SoundAction.class);
            soundAction.init(spawnSound, SPAWN_MIXDOWN_VOLUME);
            addAction(soundAction);
        }

        addAction(Actions.parallel(Actions.moveTo(x, y), Actions.scaleTo(1, 1)));
    }

    @Override
    public void onMoved(int x, int y) {
        ParallelAction parallelAction = Actions.parallel(Actions.moveTo(x, y, stepTime));

        if (isSoundEnabled() && steps % STEP_MIXDOWN_THROTTLE == 0) {
            final SoundAction soundAction = Actions.action(SoundAction.class);
            soundAction.init(stepSound, STEP_MIXDOWN_VOLUME);
            parallelAction.addAction(soundAction);
        }

        steps++;

        addAction(parallelAction);
    }

    @Override
    public void onCollided(int x, int y) {
        if(isSoundEnabled()) {
            final SoundAction soundAction = Actions.action(SoundAction.class);
            soundAction.init(collideSound, COLLIDE_MIXDOWN_VOLUME);
            addAction(soundAction);
        }
    }

    @Override
    public void onEnded(int x, int y) {
        if(isSoundEnabled()) {
            final SoundAction soundAction = Actions.action(SoundAction.class);
            soundAction.init(finishSound, FINISH_MIXDOWN_VOLUME);
            addAction(soundAction);
        }

        addAction(new PlayerFinishedAction(messages));
    }

    @Override
    public void onPickup(int x, int y, int color) {
        final int colorIdToSoundId = color - 1;
        if (isSoundEnabled() && colorIdToSoundId >= 0) {
            SoundAction soundAction = Actions.action(SoundAction.class);
            soundAction.init(pickupSounds.get(colorIdToSoundId), PICKUP_MIXDOWN_VOLUME);
            addAction(soundAction);
        }

        PlayerPickupAction pickupAction = Actions.action(PlayerPickupAction.class);
        pickupAction.init(messages, color);
        addAction(pickupAction);
    }

    @Override
    public void onTeleport(int x, int y, boolean jump) {
        if(isSoundEnabled()) {
            final SoundAction soundAction = Actions.action(SoundAction.class);
            soundAction.init(jump ? jumpSound : fallSound, jump ? JUMP_MIXDOWN_VOLUME : FALL_MIXDOWN_VOLUME);
            addAction(soundAction);
        }

        addAction(Actions.sequence(Actions.fadeOut(0.1f), Actions.moveTo(x, y, 0.2f), Actions.fadeIn(0.1f)));
    }

    private boolean isSoundEnabled() {
        return preferences.getBoolean(Settings.SOUND, true);
    }
}
