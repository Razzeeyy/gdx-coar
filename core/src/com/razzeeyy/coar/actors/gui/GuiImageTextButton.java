package com.razzeeyy.coar.actors.gui;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Scaling;

/**
 * Created by razze on 08.07.2017.
 */

public final class GuiImageTextButton extends GuiButton {
    public final Image image;
    public final Label label;

    public GuiImageTextButton(AssetManager assets, String imageAssetName, String text, float pad) {
        super(assets);

        image = new Image(assets.get(imageAssetName, Texture.class));
        image.setScaling(Scaling.fit);
        pad(pad);
        add(image).expand().fill();

        row();

        label = new GuiText(assets, text, Color.WHITE.cpy());
        add(label);
    }

    public GuiImageTextButton(AssetManager assets, String imageAssetName, String text) {
        this(assets, imageAssetName, text, 8);
    }
}
