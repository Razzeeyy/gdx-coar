package com.razzeeyy.coar.actors.gui;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.razzeeyy.coar.constants.Assets;

/**
 * Created by razze on 08.07.2017.
 */

public class GuiButton extends Button {
    public GuiButton(AssetManager assets, boolean sound) {
        super(new ButtonStyle());

        final TextureRegionDrawable buttonDrawable = new TextureRegionDrawable(new TextureRegion(assets.get(Assets.IMAGE_BUTTON, Texture.class)));
        final ButtonStyle buttonStyle = getStyle();
        buttonStyle.up = buttonDrawable;
        buttonStyle.down = buttonDrawable;
        buttonStyle.checked = buttonDrawable;
        setStyle(buttonStyle);

        setSize(getPrefWidth(), getPrefHeight());

        if (sound) {
            final Sound buttonSound = assets.get(Assets.SOUND_BUTTON, Sound.class);

            addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    buttonSound.play();
                }
            });
        }
    }

    public GuiButton(AssetManager assets) {
        this(assets, true);
    }
}
