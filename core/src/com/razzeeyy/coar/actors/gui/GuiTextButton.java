package com.razzeeyy.coar.actors.gui;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Created by razze on 08.07.2017.
 */

public final class GuiTextButton extends GuiButton {
    public final Label label;

    public GuiTextButton(AssetManager assets, String text, Color color) {
        super(assets);

        label = new GuiText(assets, text, color);
        add(label).expand().center();
    }

    public GuiTextButton(AssetManager assets, String text) {
        this(assets, text, Color.WHITE.cpy());
    }
}
