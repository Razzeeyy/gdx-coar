package com.razzeeyy.coar.actors.gui;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;

/**
 * Created by razze on 08.07.2017.
 */

public final class GuiImageButton extends GuiButton {
    public final Image image;

    public GuiImageButton(AssetManager assets, String imageAssetName, float pad) {
        super(assets);

        image = new Image(assets.get(imageAssetName, Texture.class));
        image.setScaling(Scaling.fit);
        pad(pad);
        add(image).expand().fill();
    }

    public GuiImageButton(AssetManager assets, String imageAssetName) {
        this(assets, imageAssetName, 4);
    }
}
