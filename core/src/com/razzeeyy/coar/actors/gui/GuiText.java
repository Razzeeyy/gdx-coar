package com.razzeeyy.coar.actors.gui;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.razzeeyy.coar.constants.Assets;

/**
 * Created by razze on 08.07.2017.
 */

public class GuiText extends Label {
    public GuiText(AssetManager assets, CharSequence text, Color color) {
        super(text, new LabelStyle(new BitmapFont(), color));

        final LabelStyle labelStyle = getStyle();
        labelStyle.font.dispose();
        labelStyle.font = assets.get(Assets.FONT, BitmapFont.class);
        labelStyle.fontColor = color;
        setStyle(labelStyle);
    }
}
