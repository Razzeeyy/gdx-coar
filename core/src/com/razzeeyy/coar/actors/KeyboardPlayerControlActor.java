package com.razzeeyy.coar.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by razze on 12.05.2017.
 */

public final class KeyboardPlayerControlActor extends Actor {
    private PlayerActor player;

    public KeyboardPlayerControlActor(PlayerActor player) {
        this.player = player;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
            player.setInput(-1, 0);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
            player.setInput(1, 0);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            player.setInput(0, 1);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            player.setInput(0, -1);
        }
    }
}
