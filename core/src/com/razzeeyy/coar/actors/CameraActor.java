package com.razzeeyy.coar.actors;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.razzeeyy.coar.constants.World;

/**
 * Created by razze on 11.05.2017.
 */

public final class CameraActor extends Actor {
    private final float speed = World.CAMERA_SPEED;
    private Camera camera;
    private Actor follow;
    private float wait = 0;

    public CameraActor(Camera camera, Actor follow) {
        this.camera = camera;
        this.follow = follow;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (wait > 0) {
            wait -= delta;
            return;
        }

        final float lerpFactor = speed * delta;

        setX(MathUtils.lerp(getX(), follow.getX(), lerpFactor));
        setY(MathUtils.lerp(getY(), follow.getY(), lerpFactor));
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        camera.position.x = x;
        camera.position.y = y;
    }

    @Override
    public float getX() {
        return camera.position.x;
    }

    @Override
    public void setX(float x) {
        super.setX(x);
        camera.position.x = x;
    }

    @Override
    public float getY() {
        return camera.position.y;
    }

    @Override
    public void setY(float y) {
        super.setY(y);
        camera.position.y = y;
    }

    public void showPosition(float x, float y, float time) {
        setPosition(x, y);
        wait = time;
    }

    public void showPosition(Vector2 position, float time) {
        showPosition(position.x, position.y, time);
    }
}
