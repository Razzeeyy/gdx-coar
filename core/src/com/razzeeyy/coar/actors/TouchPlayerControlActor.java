package com.razzeeyy.coar.actors;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

/**
 * Created by razze on 29.05.2017.
 */

public final class TouchPlayerControlActor extends Image {
    private final PlayerActor playerActor;

    private final ActorGestureListener actorGestureListener = new ActorGestureListener() {
        @Override
        public void fling(InputEvent event, float velocityX, float velocityY, int button) {
            final float xy = Math.abs(velocityX) - Math.abs(velocityY);
            if (xy > 0) {
                playerActor.setInput((int) Math.signum(velocityX), 0);
            } else if (xy < 0) {
                playerActor.setInput(0, (int) Math.signum(velocityY));
            }
        }
    };

    public TouchPlayerControlActor(PlayerActor playerActor) {
        super();
        setFillParent(true);
        this.playerActor = playerActor;
        addListener(actorGestureListener);
    }
}
