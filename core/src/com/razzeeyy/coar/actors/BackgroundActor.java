package com.razzeeyy.coar.actors;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.razzeeyy.coar.constants.Assets;
import com.razzeeyy.coar.constants.Events;

/**
 * Created by razze on 22.05.2017.
 */

public final class BackgroundActor extends Image implements Telegraph {
    private final MessageDispatcher messages;
    private final Drawable[] colorDrawables;
    private final CameraActor cameraActor;

    public BackgroundActor(AssetManager assets, MessageDispatcher messages, CameraActor cameraActor) {
        this.messages = messages;
        this.cameraActor = cameraActor;

        setFillParent(true);

        colorDrawables = new Drawable[4];
        colorDrawables[0] = new TextureRegionDrawable(new TextureRegion(assets.get(Assets.SPRITE_FLOOR, Texture.class)));
        colorDrawables[1] = new TextureRegionDrawable(new TextureRegion(assets.get(Assets.SPRITE_DOOR1, Texture.class)));
        colorDrawables[2] = new TextureRegionDrawable(new TextureRegion(assets.get(Assets.SPRITE_DOOR2, Texture.class)));
        colorDrawables[3] = new TextureRegionDrawable(new TextureRegion(assets.get(Assets.SPRITE_DOOR3, Texture.class)));
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);

        if (stage != null) {
            messages.addListener(this, Events.PLAYER_PICKUP);
        } else {
            messages.removeListener(this, Events.PLAYER_PICKUP);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        setPosition(cameraActor.getX(), cameraActor.getY(), Align.center);
    }

    public void changeColor(int color) {
        setDrawable(colorDrawables[color]);
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        if (msg.message == Events.PLAYER_PICKUP) {
            changeColor((Integer) msg.extraInfo);
            return true;
        }
        return false;
    }
}
