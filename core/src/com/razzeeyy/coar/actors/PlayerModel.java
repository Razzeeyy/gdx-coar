package com.razzeeyy.coar.actors;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.razzeeyy.coar.constants.Debug;
import com.razzeeyy.coar.constants.Settings;

/**
 * Created by razze on 05.05.2017.
 */

public final class PlayerModel extends Actor {
    private final Vector2 input = new Vector2();
    private final float stepTime;
    private Preferences preferences;
    private final TiledMapActor map;
    private final ShapeRenderer renderer;
    private final Vector2 spawn;
    private PlayerModelListener listener;
    private float timeSinceLastStep;
    private int color = 0;
    private boolean ended;

    public PlayerModel(Preferences preferences, TiledMapActor map, float speed) {
        this.preferences = preferences;
        this.map = map;
        stepTime = 1 / speed;

        renderer = new ShapeRenderer();

        spawn = map.getStartPosition();
    }

    public void setListener(PlayerModelListener listener) {
        this.listener = listener;
    }

    public void respawn() {
        ended = false;
        final int spawnX = (int) spawn.x;
        final int spawnY = (int) spawn.y;
        setPosition(spawnX, spawnY);
        listener.onSpawned(spawnX, spawnY);
        input.setZero();
        color = 0;
        listener.onPickup(spawnX, spawnY, color);
    }

    public void setInput(int x, int y) {
        if (MathUtils.isEqual(input.len2(), 0)) {
            input.set(x, y);

            final boolean controlInverted = preferences.getBoolean(Settings.CONTROL, false);
            if(controlInverted) {
                input.scl(-1);
            }
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (ended) { //if we finished no more events... at all and stop moving
            timeSinceLastStep = 0;
            return;
        }

        timeSinceLastStep += delta;

        while (timeSinceLastStep >= stepTime) {
            final int x = (int) getX();
            final int y = (int) getY();

            if (map.isFall(x, y)) {
                fall();
                return;
            }

            if (map.isJump(x, y)) {
                if (map.isUp(x, y)) jumpUp(x, y);
                if (map.isDown(x, y)) jumpDown(x, y);
                if (map.isLeft(x, y)) jumpLeft(x, y);
                if (map.isRight(x, y)) jumpRight(x, y);
                return;
            }

            if (map.isDoubleJump(x, y)) {
                if (map.isUp(x, y)) doubleJumpUp(x, y);
                if (map.isDown(x, y)) doubleJumpDown(x, y);
                if (map.isLeft(x, y)) doubleJumpLeft(x, y);
                if (map.isRight(x, y)) doubleJumpRight(x, y);
                return;
            }

            if (map.isEnd(x, y)) {
                ended = true;
                input.setZero();
                listener.onEnded(x, y);
                return;
            }

            if (map.isPickup(x, y)) {
                final int pickupColor = map.getTileColor(x, y);
                if (color != pickupColor) {
                    color = pickupColor;
                    listener.onPickup(x, y, color);
                }
            }

            final int newX = (int) (x + input.x);
            final int newY = (int) (y + input.y);

            if (!isInputZero()) {

                if (map.isWall(newX, newY) || (map.isDoor(newX, newY) && map.getTileColor(newX, newY) != color)) {
                    input.setZero();
                    listener.onCollided(newX, newY);
                } else {
                    setPosition(newX, newY);
                    listener.onMoved(newX, newY);
                }

            }

            timeSinceLastStep -= stepTime;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (Debug.playerOutline) {
            batch.end();

            renderer.setProjectionMatrix(batch.getProjectionMatrix());
            renderer.setColor(Color.MAGENTA);
            renderer.begin(ShapeRenderer.ShapeType.Line);

            renderer.rect(getX(), getY(), 1, 1);

            renderer.end();

            batch.begin();
        }
    }

    private boolean isInputZero() {
        return MathUtils.isEqual(input.len2(), 0);
    }

    private void fall() {
        final int spawnX = (int) spawn.x;
        final int spawnY = (int) spawn.y;
        teleport(spawnX, spawnY, false);
        input.setZero();
    }

    private void teleport(int x, int y, boolean jump) {
        setPosition(x, y);
        listener.onTeleport(x, y, jump);
    }

    private void jumpUp(int x, int y) {
        teleport(x, y + 2, true);
        input.set(0, 1);
    }

    private void jumpDown(int x, int y) {
        teleport(x, y - 2, true);
        input.set(0, -1);
    }

    private void jumpLeft(int x, int y) {
        teleport(x - 2, y, true);
        input.set(-1, 0);
    }

    private void jumpRight(int x, int y) {
        teleport(x + 2, y, true);
        input.set(1, 0);
    }

    private void doubleJumpUp(int x, int y) {
        teleport(x, y + 3, true);
        input.set(0, 1);
    }

    private void doubleJumpDown(int x, int y) {
        teleport(x, y - 3, true);
        input.set(0, -1);
    }

    private void doubleJumpLeft(int x, int y) {
        teleport(x - 3, y, true);
        input.set(-1, 0);
    }

    private void doubleJumpRight(int x, int y) {
        teleport(x + 3, y, true);
        input.set(1, 0);
    }

    public interface PlayerModelListener {
        void onSpawned(int x, int y); //at x, at y

        void onMoved(int x, int y); //to x, to y

        void onCollided(int x, int y); //colided with tile at x y

        void onEnded(int x, int y);

        void onPickup(int x, int y, int color);

        void onTeleport(int x, int y, boolean jump); //to x, to y
    }
}
