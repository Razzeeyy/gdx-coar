package com.razzeeyy.coar.actors;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by razze on 03.05.2017.
 */

public final class TiledMapActor extends Actor {
    private TiledMapRenderer renderer;

    private TiledMapTileLayer mainLayer;

    public TiledMapActor(TiledMapRenderer renderer, TiledMap map) {
        this.renderer = renderer;

        mainLayer = (TiledMapTileLayer) map.getLayers().get("main");
        if (mainLayer == null)
            throw new RuntimeException("No main layer was found in map");
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        batch.end();

        batch.setColor(1, 1, 1, 1);
        renderer.setView((OrthographicCamera) getStage().getCamera());
        renderer.render();

        batch.begin();
    }

    private TiledMapTile getTile(int x, int y) {
        TiledMapTileLayer.Cell cell = mainLayer.getCell(x, y);
        if (cell != null) {
            return cell.getTile();
        }
        return null;
    }

    private int getTileProperty(int x, int y, String name) {
        final int defaultValue = 0;

        TiledMapTile tile = getTile(x, y);
        if (tile != null) {
            return tile.getProperties().get(name, defaultValue, Integer.class);
        }

        return defaultValue;
    }


    public Vector2 getStartPosition() {
        for (int x = 0; x < mainLayer.getWidth(); x++) {
            for (int y = 0; y < mainLayer.getHeight(); y++) {
                if (isStart(x, y)) {
                    return new Vector2(x, y);
                }
            }
        }

        return Vector2.Zero.cpy();
    }

    public Vector2 getEndPosition() {
        for (int x = 0; x < mainLayer.getWidth(); x++) {
            for (int y = 0; y < mainLayer.getHeight(); y++) {
                if (isEnd(x, y)) {
                    return new Vector2(x, y);
                }
            }
        }

        return Vector2.Zero.cpy();
    }

    public boolean isStart(int x, int y) {
        return getTileProperty(x, y, "start") == 1;
    }

    public boolean isEnd(int x, int y) {
        return getTileProperty(x, y, "end") == 1;
    }

    public boolean isWall(int x, int y) {
        return getTileProperty(x, y, "wall") == 1;
    }

    public boolean isFloor(int x, int y) {
        return getTileProperty(x, y, "floor") == 1;
    }

    public boolean isFall(int x, int y) {
        return getTileProperty(x, y, "fall") == 1;
    }

    public boolean isDoor(int x, int y) {
        return getTileProperty(x, y, "door") == 1;
    }

    public boolean isPickup(int x, int y) {
        return getTileProperty(x, y, "pickup") == 1;
    }

    public int getTileColor(int x, int y) {
        return getTileProperty(x, y, "color");
    }

    public boolean isJump(int x, int y) {
        return getTileProperty(x, y, "jump") == 1;
    }

    public boolean isUp(int x, int y) {
        return getTileProperty(x, y, "up") == 1;
    }

    public boolean isDown(int x, int y) {
        return getTileProperty(x, y, "down") == 1;
    }

    public boolean isLeft(int x, int y) {
        return getTileProperty(x, y, "left") == 1;
    }

    public boolean isRight(int x, int y) {
        return getTileProperty(x, y, "right") == 1;
    }

    public boolean isJumpUp(int x, int y) {
        return isJump(x, y) && isUp(x, y);
    }

    public boolean isJumpDown(int x, int y) {
        return isJump(x, y) && isDown(x, y);
    }

    public boolean isJumpLeft(int x, int y) {
        return isJump(x, y) && isLeft(x, y);
    }

    public boolean isJumpRight(int x, int y) {
        return isJump(x, y) && isRight(x, y);
    }

    public boolean isDoubleJump(int x, int y) {
        return getTileProperty(x, y, "jump2") == 1;
    }

    public boolean isDoubleJumpUp(int x, int y) {
        return isDoubleJump(x, y) && isUp(x, y);
    }

    public boolean isDoubleJumpDown(int x, int y) {
        return isDoubleJump(x, y) && isDown(x, y);
    }

    public boolean isDoubleJumpLeft(int x, int y) {
        return isDoubleJump(x, y) && isLeft(x, y);
    }

    public boolean isDoubleJumpRight(int x, int y) {
        return isDoubleJump(x, y) && isRight(x, y);
    }
}
