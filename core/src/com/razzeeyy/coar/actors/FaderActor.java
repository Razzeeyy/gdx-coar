package com.razzeeyy.coar.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by razze on 29.05.2017.
 */

public final class FaderActor extends Image {
    private static final float FADE_IN_TIME = 0.3f;
    private static final float FADE_OUT_TIME = 0.2f;

    public FaderActor(Texture texture) {
        super(texture);
        setFillParent(true);
    }

    public void fadeOut() {
        clearActions();
        addAction(Actions.sequence(Actions.fadeOut(FADE_OUT_TIME), Actions.removeActor()));
    }

    public void fadeIn(Runnable runnable) {
        clearActions();
        addAction(Actions.sequence(Actions.fadeIn(FADE_IN_TIME), Actions.run(runnable)));
    }
}
