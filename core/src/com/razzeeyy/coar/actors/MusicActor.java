package com.razzeeyy.coar.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.razzeeyy.coar.constants.Assets;
import com.razzeeyy.coar.constants.Debug;
import com.razzeeyy.coar.constants.Settings;

/**
 * Created by razze on 28.06.2017.
 */

public final class MusicActor extends Actor {

    private final float[] mixdown = {
            1f, //0
            1f, //1
            1f, //2
    };
    private final Array<Music> music;
    private int current = -1;
    private Preferences preferences;

    public MusicActor(AssetManager assets, Preferences preferences) {
        music = new Array<Music>();
        music.add(assets.get(Assets.MUSIC_MENU, Music.class));
        music.add(assets.get(Assets.MUSIC_GAME1, Music.class));
        music.add(assets.get(Assets.MUSIC_GAME2, Music.class));
        this.preferences = preferences;
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);

        if (stage == null) {
            for (Music track : music) {
                track.stop();
            }
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        checkMute();

        if (current != -1 && !music.get(current).isPlaying()) {
            playRandom();
        }

        if (Debug.allowMusicSkip && Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            playNext();
        }
    }

    public void playMenuTheme() {
        if (current == -1) {
            play(0);
        }
    }

    public void playRandom() {
        play(MathUtils.random(music.size - 1));
    }

    public void playNext() {
        play((current + 1) % music.size);
    }

    private void play(int id) {
        if (current != -1) {
            music.get(current).stop();
        }
        music.get(id).setVolume(mixdown[id]);
        music.get(id).play();
        current = id;
    }

    private void checkMute() {
        if(current == -1) {
            return;
        }

        Music currentMusic = music.get(current);

        if(preferences.getBoolean(Settings.MUSIC, true)) { //music enabled
            currentMusic.setVolume(mixdown[current]);
            currentMusic.play();
        } else {
            currentMusic.setVolume(0);
            currentMusic.stop();
        }
    }
}
