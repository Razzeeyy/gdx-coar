package com.razzeeyy.coar.actors;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Group;

/**
 * Created by razzeeyy on 05.05.17.
 */

public final class PlayerActor extends Group {
    private PlayerModel model;
    private PlayerView view;

    public PlayerActor(AssetManager assets, MessageDispatcher messages, Preferences preferences, TiledMapActor map, float speed) {
        model = new PlayerModel(preferences, map, speed);
        view = new PlayerView(assets, messages, preferences, speed);

        model.setListener(view);

        addActor(model);
        addActor(view);
    }

    @Override
    public void clear() {
        super.clear();
        model.clear();
        view.clear();
    }

    public void respawn() {
        view.clearActions();
        model.respawn();
    }

    public void setInput(int x, int y) {
        if (view.getActions().size > 0) return;
        model.setInput(x, y);
    }

    @Override
    public float getX() {
        return view.getX();
    }

    @Override
    public float getY() {
        return view.getY();
    }
}
