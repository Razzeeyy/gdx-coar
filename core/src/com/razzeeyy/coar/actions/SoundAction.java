package com.razzeeyy.coar.actions;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.Action;

/**
 * Created by razze on 06.06.2017.
 */

public final class SoundAction extends Action {
    private Sound sound;
    private float volume = 1;

    public void init(Sound sound, float volume) {
        this.sound = sound;
        this.volume = volume;
    }

    public void init(Sound sound) {
        init(sound, 1);
    }

    @Override
    public boolean act(float delta) {
        sound.stop();
        sound.play(volume);
        return true;
    }
}
