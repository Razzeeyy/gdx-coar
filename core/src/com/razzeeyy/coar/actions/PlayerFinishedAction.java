package com.razzeeyy.coar.actions;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.razzeeyy.coar.constants.Events;

/**
 * Created by razze on 19.05.2017.
 */

public final class PlayerFinishedAction extends Action {
    private final MessageDispatcher messages;

    public PlayerFinishedAction(MessageDispatcher messages) {
        this.messages = messages;
    }

    @Override
    public boolean act(float delta) {
        messages.dispatchMessage(Events.PLAYER_FINISHED);
        return true;
    }
}
