package com.razzeeyy.coar.actions;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.razzeeyy.coar.constants.Events;

/**
 * Created by razze on 19.05.2017.
 */

public final class PlayerPickupAction extends Action {
    private MessageDispatcher messages;
    private int color;

    public void init(MessageDispatcher messages, int color) {
        this.messages = messages;
        this.color = color;
    }

    @Override
    public boolean act(float delta) {
        messages.dispatchMessage(Events.PLAYER_PICKUP, (Integer) color);
        return true;
    }
}
