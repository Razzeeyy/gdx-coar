package com.razzeeyy.coar.descriptors;

import com.razzeeyy.coar.constants.Debug;

/**
 * Contains information about level in a pack
 * Created by razze on 18.05.2017.
 */

public final class LevelDescriptor {
    public final String name;
    public final String filename;
    public final boolean hidden;

    public LevelDescriptor(String name, String filename, boolean hide) {
        this.name = name;
        this.filename = filename;
        this.hidden = hide && !Debug.showHiddenLevels;
    }

    public LevelDescriptor(String name, String filename) {
        this(name, filename, false);
    }
}
