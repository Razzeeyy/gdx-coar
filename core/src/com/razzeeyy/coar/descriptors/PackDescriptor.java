package com.razzeeyy.coar.descriptors;

import com.razzeeyy.coar.constants.Debug;

/**
 * Created by razze on 18.05.2017.
 */

public final class PackDescriptor {
    public final String name;
    public final String imageFilename;
    public final LevelDescriptor[] levels = new LevelDescriptor[10];
    public final boolean hidden;

    private int size = 0;

    public PackDescriptor(String name, String imageFilename, boolean hide) {
        this.name = name;
        this.imageFilename = imageFilename;
        this.hidden = hide && !Debug.showHiddenLevels;
    }

    public PackDescriptor(String name, String imageFilename) {
        this(name, imageFilename, false);
    }

    public void addLevel(String name, String filename, boolean hide) {
        levels[size++] = new LevelDescriptor(name, filename, hide);
    }

    public void addLevel(String name, String filename) {
        addLevel(name, filename, false);
    }
}
