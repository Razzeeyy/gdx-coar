package com.razzeeyy.coar.descriptors;

import com.razzeeyy.coar.constants.Levels;

/**
 * Created by razze on 26.05.2017.
 */

public final class SelectedLevelDescriptor {
    public final int packIndex, levelIndex;
    public final PackDescriptor pack;
    public final LevelDescriptor level;

    public SelectedLevelDescriptor(int packIndex, int levelIndex) {
        this.packIndex = packIndex;
        this.levelIndex = levelIndex;
        pack = Levels.packs[packIndex];
        level = pack.levels[levelIndex];
    }

    static public String getSaveId(int packIndex, int levelIndex) {
        return packIndex + ":" + levelIndex;
    }

    public boolean hasNextLevel() {
        return levelIndex >= 0 && levelIndex + 1 < pack.levels.length && !pack.levels[levelIndex + 1].hidden;
    }

    public SelectedLevelDescriptor getNextLevel() {
        if (hasNextLevel()) {
            return new SelectedLevelDescriptor(packIndex, levelIndex + 1);
        }
        return null;
    }

    public String getSaveId() {
        return getSaveId(packIndex, levelIndex);
    }

    public int getNextLevelIndex() {
        if (!hasNextLevel()) return 0;
        return levelIndex + 1;
    }

    public String getNextLevelSaveId() {
        return getSaveId(packIndex, getNextLevelIndex());
    }
}
