package com.razzeeyy.coar;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ai.GdxAI;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.ExternalFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.razzeeyy.coar.actors.MusicActor;
import com.razzeeyy.coar.constants.Debug;
import com.razzeeyy.coar.constants.World;
import com.razzeeyy.coar.screens.PreloadScreen;

public final class CoarGame extends Game {
    public Color background;

    public Stage world;
    public Stage ui;

    public AssetManager assets;

    public MessageDispatcher messages;

    public Preferences preferences;
    public MusicActor music;
    private BitmapFont font;

    public void assetsLoaded() { //called in preload screen after all assets were preloaded
        music = new MusicActor(assets, preferences);
        world.addActor(music);
    }

    @Override
    public void create() {
        background = Color.BLACK.cpy();

        world = new Stage(new ExtendViewport(World.WIDTH, World.HEIGHT));
        ui = new Stage(new ScreenViewport());

        Gdx.input.setInputProcessor(new InputMultiplexer(ui, world));

        assets = new AssetManager(Debug.externalAssets ? new ExternalFileHandleResolver() : new InternalFileHandleResolver());

        messages = new MessageDispatcher();

        preferences = Gdx.app.getPreferences(CoarGame.class.getName());

        font = new BitmapFont();

        setScreen(new PreloadScreen(this));
    }

    @Override
    public void dispose() {
        super.dispose();

        preferences.flush();

        background.set(Color.RED);

        world.dispose();
        ui.dispose();

        assets.dispose();

        messages.clear();
    }

    @Override
    public void render() {
        final int GL_COVERAGE_BUFFER_BIT = Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0;

        Gdx.gl.glClearColor(background.r, background.g, background.b, background.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | GL_COVERAGE_BUFFER_BIT);

        GdxAI.getTimepiece().update(Gdx.graphics.getDeltaTime());
        messages.update();

        world.getViewport().apply();
        world.act();
        world.draw();

        ui.getViewport().apply();
        ui.act();
        ui.draw();

        super.render();

        if (Debug.developmentMark) {
            final Batch batch = ui.getBatch();
            batch.begin();
            font.draw(batch, "DEVELOPMENT VERSION", 32, 32);
            batch.end();
        }
    }

    @Override
    public void resize(int width, int height) {
        ui.getViewport().update(width, height, true);
        world.getViewport().update(width, height, false);

        super.resize(width, height);
    }

    public boolean isMobile() {
        return Gdx.app.getType() == Application.ApplicationType.Android;
    }

    public boolean isDesktop() {
        return Gdx.app.getType() == Application.ApplicationType.Desktop && !Debug.noDesktop;
    }

    public void sizeButton(Cell button) {
        if(isDesktop()) {
            button.width(64).height(64);
        } else {
            button.width(64 * Gdx.graphics.getDensity()).height(64 * Gdx.graphics.getDensity());
        }
    }
}
