package com.razzeeyy.coar.screens;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.razzeeyy.coar.CoarGame;
import com.razzeeyy.coar.actors.MusicActor;

/**
 * Created by razze on 02.05.2017.
 */

public abstract class AbstractScreen extends ScreenAdapter {
    protected final CoarGame game;

    protected final Color background;

    protected final Stage world;
    protected final Stage ui;

    protected final AssetManager assets;

    protected final MessageDispatcher messages;

    protected final Preferences preferences;

    protected final MusicActor music;

    public AbstractScreen(CoarGame game) {
        this.game = game;

        this.background = game.background;

        this.world = game.world;
        this.ui = game.ui;

        this.assets = game.assets;

        this.messages = game.messages;

        this.preferences = game.preferences;

        this.music = game.music;
    }

    public boolean isMobile() {
        return game.isMobile();
    }

    public boolean isDesktop() {
        return game.isDesktop();
    }
}
