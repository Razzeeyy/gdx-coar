package com.razzeeyy.coar.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.razzeeyy.coar.CoarGame;
import com.razzeeyy.coar.constants.Assets;
import com.razzeeyy.coar.constants.Colors;
import com.razzeeyy.coar.constants.Debug;
import com.razzeeyy.coar.constants.Levels;
import com.razzeeyy.coar.descriptors.PackDescriptor;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by razze on 02.05.2017.
 */

public final class PreloadScreen extends AbstractScreen {
    private final Table table;
    private float splashTime = 1; //minimum time to keep showing preload screen for

    public PreloadScreen(CoarGame game) {
        super(game);

        background.set(Colors.background);

        table = new Table();
        table.setFillParent(true);


        final BitmapFontLoader.BitmapFontParameter bitmapFontParameter = new BitmapFontLoader.BitmapFontParameter();
        bitmapFontParameter.minFilter = Texture.TextureFilter.Linear;
        bitmapFontParameter.magFilter = Texture.TextureFilter.Linear;

        assets.load(Assets.FONT, BitmapFont.class, bitmapFontParameter);

        assets.finishLoadingAsset(Assets.FONT);


        final TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        textureParameter.magFilter = Texture.TextureFilter.Linear;

        assets.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));

        assets.load(Assets.SPRITE_PLAYER, Texture.class, textureParameter);
        assets.load(Assets.SPRITE_FLOOR, Texture.class, textureParameter);
        assets.load(Assets.SPRITE_DOOR1, Texture.class, textureParameter);
        assets.load(Assets.SPRITE_DOOR2, Texture.class, textureParameter);
        assets.load(Assets.SPRITE_DOOR3, Texture.class, textureParameter);

        assets.load(Assets.IMAGE_BUTTON, Texture.class, textureParameter);
        assets.load(Assets.IMAGE_RESTART, Texture.class, textureParameter);
        assets.load(Assets.IMAGE_PIXEL, Texture.class, textureParameter);
        assets.load(Assets.IMAGE_SETTINGS, Texture.class, textureParameter);
        assets.load(Assets.IMAGE_MUSIC, Texture.class, textureParameter);
        assets.load(Assets.IMAGE_SOUND, Texture.class, textureParameter);
        assets.load(Assets.IMAGE_CONTROL, Texture.class, textureParameter);

        assets.load(Assets.SOUND_PICKUP_1, Sound.class);
        assets.load(Assets.SOUND_PICKUP_2, Sound.class);
        assets.load(Assets.SOUND_PICKUP_3, Sound.class);
        assets.load(Assets.SOUND_JUMP, Sound.class);
        assets.load(Assets.SOUND_BUMP, Sound.class);
        assets.load(Assets.SOUND_FALL, Sound.class);
        assets.load(Assets.SOUND_STEP, Sound.class);
        assets.load(Assets.SOUND_SPAWN, Sound.class);
        assets.load(Assets.SOUND_FINISH, Sound.class);

        assets.load(Assets.SOUND_BUTTON, Sound.class);

        final ParticleEffectLoader.ParticleEffectParameter particleEffectParameter = new ParticleEffectLoader.ParticleEffectParameter();
        particleEffectParameter.imagesDir = Gdx.files.internal("sprites");
        assets.load(Assets.PARTICLE_TRAIL, ParticleEffect.class, particleEffectParameter);
        assets.load(Assets.PARTICLE_PICKUP, ParticleEffect.class, particleEffectParameter);

        assets.load(Assets.MUSIC_MENU, Music.class);
        assets.load(Assets.MUSIC_GAME1, Music.class);
        assets.load(Assets.MUSIC_GAME2, Music.class);

        for (PackDescriptor pack : Levels.packs) {
            assets.load(pack.imageFilename, Texture.class, textureParameter);
        }


        table.add(createAuthorLabel("KOLO")).pad(16);
//        table.add(createAuthorLabel("Razzeeyy")).pad(16);
//        table.row();
//        table.add(createAuthorLabel("FieryPenguin")).pad(16);
//        table.row();
//        table.add(createAuthorLabel("Music By")).padTop(32).padBottom(8);
//        table.row();
//        table.add(createAuthorLabel("Chris Zabriskie"));
    }

    @Override
    public void show() {
        super.show();
        ui.addActor(table);
    }

    @Override
    public void hide() {
        super.hide();
        table.remove();
    }

    @Override
    public void dispose() {
        super.dispose();
        table.clear();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        splashTime -= delta;

        if (assets.update() && splashTime < 0) { //if all assets loaded

            game.assetsLoaded();

            if (Debug.development) {
                this.dispose();
                throw new NotImplementedException();
            } else {
                game.setScreen(new PackSelectScreen(game));
                this.dispose();
            }

        }
    }

    private Label createAuthorLabel(String text) {
        final Label.LabelStyle labelStyle = new Label.LabelStyle(assets.get(Assets.FONT, BitmapFont.class), Color.BLACK.cpy());
        final Label authorLabel = new Label(text, labelStyle);

        return authorLabel;
    }
}
