package com.razzeeyy.coar.screens;

import com.razzeeyy.coar.CoarGame;
import com.razzeeyy.coar.constants.Debug;
import com.razzeeyy.coar.descriptors.SelectedLevelDescriptor;

/**
 * Created by razze on 26.05.2017.
 */

public final class GameEndScreen extends AbstractScreen {
    private final SelectedLevelDescriptor selectedLevel;

    public GameEndScreen(CoarGame game, SelectedLevelDescriptor selectedLevel) {
        super(game);
        this.selectedLevel = selectedLevel;
    }

    @Override
    public void show() {
        super.show();

        background.set(1, 1, 1, 1);
        //TODO: interstitial ads
        SelectedLevelDescriptor nextSelectedLevel = selectedLevel.getNextLevel();
        if (nextSelectedLevel != null) {
            if (!Debug.unlockAllLevels)
                preferences.putBoolean(selectedLevel.getNextLevelSaveId(), true); //unlock the next level
            game.setScreen(new LevelLoadScreen(game, nextSelectedLevel));
            this.dispose();
        } else {
            game.setScreen(new PackSelectScreen(game, true));
            this.dispose();
        }
    }

    @Override
    public void hide() {
        super.hide();
    }

    @Override
    public void dispose() {
        super.dispose();
        preferences.flush();
    }
}
