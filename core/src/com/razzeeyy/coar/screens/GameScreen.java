package com.razzeeyy.coar.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.razzeeyy.coar.CoarGame;
import com.razzeeyy.coar.actors.BackButtonActor;
import com.razzeeyy.coar.actors.BackgroundActor;
import com.razzeeyy.coar.actors.CameraActor;
import com.razzeeyy.coar.actors.FaderActor;
import com.razzeeyy.coar.actors.KeyboardPlayerControlActor;
import com.razzeeyy.coar.actors.ParticlesActor;
import com.razzeeyy.coar.actors.PlayerActor;
import com.razzeeyy.coar.actors.TiledMapActor;
import com.razzeeyy.coar.actors.TouchPlayerControlActor;
import com.razzeeyy.coar.actors.gui.GuiImageButton;
import com.razzeeyy.coar.actors.gui.GuiText;
import com.razzeeyy.coar.constants.Assets;
import com.razzeeyy.coar.constants.Colors;
import com.razzeeyy.coar.constants.Debug;
import com.razzeeyy.coar.constants.Events;
import com.razzeeyy.coar.constants.World;
import com.razzeeyy.coar.descriptors.SelectedLevelDescriptor;

/**
 * Created by razze on 02.05.2017.
 */

public final class GameScreen extends AbstractScreen implements Telegraph {
    private final SelectedLevelDescriptor selectedLevel;
    private final TiledMap map;
    private final OrthogonalTiledMapRenderer tiledMapRenderer;
    private final TiledMapActor mapActor;
    private final PlayerActor playerActor;
    private final CameraActor cameraActor;
    private final KeyboardPlayerControlActor keyboardPlayerControlActor;
    private final BackgroundActor backgroundActor;
    private final TouchPlayerControlActor touchPlayerControlActor;
    private final ParticlesActor particlesActor;

    private final float CAMERA_WAIT = 1f;
    private final Vector2 mapEndPosition;

    private final Table table;
    private final Button restartButton;
    private final FaderActor faderActor;
    private final BackButtonActor backButtonActor;
    private final Label levelLabel;

    public GameScreen(CoarGame game, SelectedLevelDescriptor selectedLevel) {
        super(game);

        background.set(Colors.background);

        this.selectedLevel = selectedLevel;
        map = assets.get(selectedLevel.level.filename, TiledMap.class);

        tiledMapRenderer = new OrthogonalTiledMapRenderer(map, 1 / World.UNIT, world.getBatch());
        mapActor = new TiledMapActor(tiledMapRenderer, map);
        playerActor = new PlayerActor(assets, messages, preferences, mapActor, World.PLAYER_SPEED);
        playerActor.respawn();
        cameraActor = new CameraActor(world.getCamera(), playerActor);
        keyboardPlayerControlActor = new KeyboardPlayerControlActor(playerActor);
        backgroundActor = new BackgroundActor(assets, messages, cameraActor);
        touchPlayerControlActor = new TouchPlayerControlActor(playerActor);
        particlesActor = new ParticlesActor(assets, messages, playerActor);

        mapEndPosition = mapActor.getEndPosition();

        table = new Table();
        table.setDebug(Debug.guiOutline);
        table.pad(4);
        table.setFillParent(true);

        restartButton = new GuiImageButton(assets, Assets.IMAGE_RESTART);
        game.sizeButton(table.add(restartButton).align(Align.topLeft));
        restartButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                playerActor.respawn();
                cameraActor.showPosition(mapEndPosition, CAMERA_WAIT);
                levelLabel.addAction(Actions.sequence(Actions.show(), Actions.fadeIn(0), Actions.fadeOut(1f), Actions.hide()));
            }
        });

        faderActor = new FaderActor(assets.get(Assets.IMAGE_PIXEL, Texture.class));
        backButtonActor = new BackButtonActor();
        backButtonActor.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                switchToLevelSelect();
            }
        });

        levelLabel = new GuiText(assets, selectedLevel.pack.name + " " + selectedLevel.level.name, Color.BLACK.cpy());
        levelLabel.setTouchable(Touchable.disabled);
        table.row();
        table.add(levelLabel).expand().center();
    }

    @Override
    public void show() {
        super.show();

        cameraActor.showPosition(mapEndPosition, CAMERA_WAIT);

        world.addActor(cameraActor); //NOTE: order matters here, it defines act() sequence
        world.addActor(backgroundActor);
        world.addActor(mapActor);
        world.addActor(particlesActor);
        world.addActor(playerActor);
        world.addActor(keyboardPlayerControlActor);
        ui.addActor(touchPlayerControlActor);

        ui.addActor(table);
        ui.addActor(faderActor);
        faderActor.fadeOut();
        ui.addActor(backButtonActor);
        levelLabel.addAction(Actions.sequence(Actions.fadeOut(1f), Actions.hide()));

        messages.addListener(this, Events.PLAYER_FINISHED);
    }

    @Override
    public void hide() {
        super.hide();
        mapActor.remove();
        playerActor.remove();
        cameraActor.remove();
        keyboardPlayerControlActor.remove();
        backgroundActor.remove();
        touchPlayerControlActor.remove();
        particlesActor.remove();

        table.remove();
        faderActor.remove();
        backButtonActor.remove();

        messages.removeListener(this, Events.PLAYER_FINISHED);
    }

    @Override
    public void dispose() {
        super.dispose();
        assets.unload(selectedLevel.level.filename); //make sure to unload current level
        tiledMapRenderer.dispose();

        mapActor.clear();
        playerActor.clear();
        cameraActor.clear();
        keyboardPlayerControlActor.clear();
        backgroundActor.clear();
        touchPlayerControlActor.clear();
        particlesActor.clear();

        table.clear();
        restartButton.clear();
        faderActor.clear();
        backButtonActor.clearActions();
        levelLabel.clear();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        table.invalidate();
    }

    public void switchToLevelSelect() {
        game.setScreen(new LevelSelectScreen(game, selectedLevel.packIndex));
        this.dispose();
    }

    public void switchToGameEnd() {
        game.setScreen(new GameEndScreen(game, selectedLevel));
        this.dispose();
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        if (msg.message == Events.PLAYER_FINISHED) {
            ui.addActor(faderActor);
            faderActor.fadeIn(new Runnable() {
                @Override
                public void run() {
                    switchToGameEnd();
                }
            });
            return true;
        }
        return false;
    }
}
