package com.razzeeyy.coar.screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.razzeeyy.coar.CoarGame;
import com.razzeeyy.coar.actors.FaderActor;
import com.razzeeyy.coar.constants.Assets;
import com.razzeeyy.coar.descriptors.SelectedLevelDescriptor;

/**
 * Created by razze on 14.05.2017.
 */

public final class LevelLoadScreen extends AbstractScreen {
    private final SelectedLevelDescriptor selectedLevel;

    private final FaderActor faderActor;

    public LevelLoadScreen(CoarGame game, SelectedLevelDescriptor selectedLevel) {
        super(game);

        background.set(1, 1, 1, 1);

        this.selectedLevel = selectedLevel;

        final TmxMapLoader.Parameters tmxMapLoaderParameters = new TmxMapLoader.Parameters();
        tmxMapLoaderParameters.textureMinFilter = Texture.TextureFilter.Linear;
        tmxMapLoaderParameters.textureMagFilter = Texture.TextureFilter.Linear;

        assets.load(selectedLevel.level.filename, TiledMap.class, tmxMapLoaderParameters);

        faderActor = new FaderActor(assets.get(Assets.IMAGE_PIXEL, Texture.class));
    }

    @Override
    public void show() {
        super.show();

        ui.addActor(faderActor);
        faderActor.fadeOut();
    }

    @Override
    public void hide() {
        super.hide();
        faderActor.remove();
    }

    @Override
    public void dispose() {
        super.dispose();
        faderActor.clear();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        if (assets.update()) { //when map is done loading
            game.setScreen(new GameScreen(game, selectedLevel));
            this.dispose();
        }
    }
}
