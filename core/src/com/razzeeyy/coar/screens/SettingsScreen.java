package com.razzeeyy.coar.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.razzeeyy.coar.CoarGame;
import com.razzeeyy.coar.actors.BackButtonActor;
import com.razzeeyy.coar.actors.gui.GuiImageButton;
import com.razzeeyy.coar.actors.gui.GuiText;
import com.razzeeyy.coar.constants.Assets;
import com.razzeeyy.coar.constants.Debug;
import com.razzeeyy.coar.constants.Settings;

/**
 * Created by razze on 27.09.2017.
 */

public final class SettingsScreen extends AbstractScreen {
    private final Table table;
    private final GuiImageButton soundButton;
    private final GuiText soundLabel;
    private final GuiImageButton musicButton;
    private final GuiText musicLabel;
    private final GuiImageButton controlButton;
    private final GuiText controlLabel;
    private final BackButtonActor backButtonActor;

    public SettingsScreen(CoarGame game) {
        super(game);

        final float PAD_TOP = 10;
        final float PAD_BOTTOM = 10;

        table = new Table();
        table.setFillParent(true);
        table.setDebug(Debug.guiOutline);

        table.row().padTop(PAD_TOP).padBottom(PAD_BOTTOM);

        soundButton = new GuiImageButton(assets, Assets.IMAGE_SOUND);
        soundButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                boolean sound = preferences.getBoolean(Settings.SOUND, true);
                sound = !sound;
                preferences.putBoolean(Settings.SOUND, sound);
                preferences.flush();
                soundLabel.setText(sound ? "ON" : "OFF");
            }
        });
        game.sizeButton(table.add(soundButton));
        soundLabel = new GuiText(assets, preferences.getBoolean(Settings.SOUND, true) ? "ON" : "OFF", Color.BLACK.cpy());
        table.add(soundLabel);

        table.row().padTop(PAD_TOP).padBottom(PAD_BOTTOM);

        musicButton = new GuiImageButton(assets, Assets.IMAGE_MUSIC);
        musicButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                boolean music = preferences.getBoolean(Settings.MUSIC, true);
                music = !music;
                preferences.putBoolean(Settings.MUSIC, music);
                preferences.flush();
                musicLabel.setText(music ? "ON" : "OFF");
            }
        });
        game.sizeButton(table.add(musicButton));
        musicLabel = new GuiText(assets, preferences.getBoolean(Settings.MUSIC, true) ? "ON" : "OFF", Color.BLACK.cpy());
        table.add(musicLabel);

        table.row().padTop(PAD_TOP).padBottom(PAD_BOTTOM);

        controlButton = new GuiImageButton(assets, Assets.IMAGE_CONTROL);
        controlButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                boolean control = preferences.getBoolean(Settings.CONTROL, false);
                control = !control;
                preferences.putBoolean(Settings.CONTROL, control);
                preferences.flush();
                controlLabel.setText(control ? "INVERT" : "NORMAL");
            }
        });
        game.sizeButton(table.add(controlButton));
        controlLabel = new GuiText(assets, preferences.getBoolean(Settings.CONTROL, false) ? "INVERT" : "NORMAL", Color.BLACK.cpy());
        table.add(controlLabel).width(controlLabel.getPrefWidth());

        backButtonActor = new BackButtonActor();
        backButtonActor.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                switchToPackSelect();
            }
        });
    }

    @Override
    public void show() {
        super.show();
        ui.addActor(table);
        ui.addActor(backButtonActor);
    }

    @Override
    public void hide() {
        super.hide();
        table.remove();
        backButtonActor.remove();
    }

    @Override
    public void dispose() {
        super.dispose();
        table.clear();
        soundButton.clear();
        soundLabel.clear();
        musicButton.clear();
        musicLabel.clear();
        controlButton.clear();
        controlLabel.clear();
        backButtonActor.clearActions();
    }

    public void switchToPackSelect() {
        game.setScreen(new PackSelectScreen(game));
        this.dispose();
    }
}
