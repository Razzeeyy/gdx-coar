package com.razzeeyy.coar.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.razzeeyy.coar.CoarGame;
import com.razzeeyy.coar.actors.BackButtonActor;
import com.razzeeyy.coar.actors.FaderActor;
import com.razzeeyy.coar.actors.gui.GuiImageButton;
import com.razzeeyy.coar.actors.gui.GuiImageTextButton;
import com.razzeeyy.coar.constants.Assets;
import com.razzeeyy.coar.constants.Colors;
import com.razzeeyy.coar.constants.Debug;
import com.razzeeyy.coar.constants.Levels;
import com.razzeeyy.coar.descriptors.PackDescriptor;

/**
 * Created by razze on 14.05.2017.
 */

public final class PackSelectScreen extends AbstractScreen {
    private final Table table;
    private final Table packTable;
    private final GuiImageButton settingsButton;
    private final ScrollPane packScrollPane;
    private final BackButtonActor backButtonActor;
    private final FaderActor faderActor;
    private final boolean fader;

    public PackSelectScreen(CoarGame game, boolean fader) {
        super(game);

        this.fader = fader;

        background.set(Colors.background);

        table = new Table();
        table.setFillParent(true);
        table.setDebug(Debug.guiOutline);

        settingsButton = new GuiImageButton(assets, Assets.IMAGE_SETTINGS);
        settingsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                switchToSettings();
            }
        });
        game.sizeButton(table.add(settingsButton).align(Align.topRight));
        table.row();

        packTable = new Table();
        packTable.setDebug(Debug.guiOutline);

        for (int i = 0; i < Levels.packs.length; i++) {
            final PackDescriptor pack = Levels.packs[i];

            if (pack.hidden) continue;

            final GuiImageTextButton packButton = new GuiImageTextButton(assets, pack.imageFilename, pack.name);
            packButton.label.setFontScale(0.8f);
            packButton.addListener(new PackSelectListener(i));

            packButton.setDebug(Debug.guiOutline);
            Cell cell = packTable.add(packButton).pad(16).height(Value.percentHeight(0.6f, table)).width(Value.percentWidth(0.5f, table));
            if (isDesktop()) cell.width(256);
        }

        packScrollPane = new ScrollPane(packTable);
        packScrollPane.setDebug(Debug.guiOutline);

        table.add(packScrollPane).expand().fill();

        backButtonActor = new BackButtonActor();
        backButtonActor.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });

        faderActor = new FaderActor(assets.get(Assets.IMAGE_PIXEL, Texture.class));
    }

    public PackSelectScreen(CoarGame game) {
        this(game, false);
    }

    @Override
    public void show() {
        super.show();
        ui.addActor(table);
        ui.addActor(backButtonActor);
        if (fader) {
            ui.addActor(faderActor);
        }
        faderActor.fadeOut();

        music.playMenuTheme();
    }

    @Override
    public void hide() {
        super.hide();
        table.remove();
        backButtonActor.remove();
        faderActor.remove();
    }

    @Override
    public void dispose() {
        super.dispose();
        table.clear();
        packTable.clear();
        packScrollPane.clear();
        backButtonActor.clear();
        faderActor.clear();
        settingsButton.clearChildren();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        packTable.invalidateHierarchy();
    }

    public void switchToSettings() {
        game.setScreen(new SettingsScreen(game));
        this.dispose();
    }

    public void switchToLevelSelect(int packIndex) {
        game.setScreen(new LevelSelectScreen(game, packIndex));
        this.dispose();
    }

    private class PackSelectListener extends ChangeListener {
        private final int packIndex;

        public PackSelectListener(int packIndex) {
            this.packIndex = packIndex;
        }

        @Override
        public void changed(ChangeEvent event, Actor actor) {
            switchToLevelSelect(this.packIndex);
        }
    }
}
