package com.razzeeyy.coar.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.razzeeyy.coar.CoarGame;
import com.razzeeyy.coar.actors.BackButtonActor;
import com.razzeeyy.coar.actors.gui.GuiText;
import com.razzeeyy.coar.actors.gui.GuiTextButton;
import com.razzeeyy.coar.constants.Colors;
import com.razzeeyy.coar.constants.Debug;
import com.razzeeyy.coar.constants.Levels;
import com.razzeeyy.coar.descriptors.LevelDescriptor;
import com.razzeeyy.coar.descriptors.PackDescriptor;
import com.razzeeyy.coar.descriptors.SelectedLevelDescriptor;

/**
 * Created by razze on 14.05.2017.
 */

public final class LevelSelectScreen extends AbstractScreen {
    private final int packIndex;
    private final PackDescriptor pack;
    private final Table table;
    private final BackButtonActor backButtonActor;

    public LevelSelectScreen(CoarGame game, int packIndex) {
        super(game);
        this.packIndex = packIndex;
        pack = Levels.packs[packIndex];

        background.set(Colors.background);

        table = new Table();
        table.setDebug(Debug.guiOutline);
        table.setFillParent(true);

        if (isDesktop()) table.setFillParent(false);

        table.add(new GuiText(assets, pack.name, Color.BLACK.cpy())).colspan(2);

        for (int i = 0; i < pack.levels.length; i++) {
            final LevelDescriptor level = pack.levels[i];

            if (i % 2 == 0) table.row();

            if (level.hidden) continue;

            GuiTextButton levelButton;
            if (Debug.unlockAllLevels || i == 0 || preferences.getBoolean(SelectedLevelDescriptor.getSaveId(packIndex, i), false)) { //if level is unlocked
                levelButton = new GuiTextButton(assets, level.name);
            } else { //level is locked
                levelButton = new GuiTextButton(assets, level.name);
                levelButton.label.setColor(Color.BLACK.cpy());
                levelButton.setTouchable(Touchable.disabled);
            }
            table.add(levelButton).expand().pad(16f).width(Value.percentWidth(1 / 3f, table)).height(Value.percentHeight(1 / 8f, table));
            levelButton.addListener(new LevelButtonClick(new SelectedLevelDescriptor(packIndex, i)));
        }

        backButtonActor = new BackButtonActor();
        backButtonActor.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                switchToPackSelect();
            }
        });
    }

    @Override
    public void show() {
        super.show();
        ui.addActor(table);
        ui.addActor(backButtonActor);
    }

    @Override
    public void hide() {
        super.hide();
        table.remove();
        backButtonActor.remove();
    }

    @Override
    public void dispose() {
        super.dispose();
        table.clear();
        backButtonActor.clearActions();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);

        if (isDesktop()) {
            table.setSize(width / 2, height);
            table.setPosition((width - table.getWidth()) / 2, (height - table.getHeight()) / 2);
        }

        table.invalidate();
    }

    public void switchToPackSelect() {
        game.setScreen(new PackSelectScreen(game));
        this.dispose();
    }

    public void switchToLevelLoad(SelectedLevelDescriptor selectedLevel) {
        game.setScreen(new LevelLoadScreen(game, selectedLevel));
        this.dispose();
    }

    private class LevelButtonClick extends ChangeListener {
        private final SelectedLevelDescriptor selectedLevel;

        public LevelButtonClick(SelectedLevelDescriptor selectedLevel) {
            this.selectedLevel = selectedLevel;
        }

        @Override
        public void changed(ChangeEvent event, Actor actor) {
            switchToLevelLoad(selectedLevel);
        }
    }
}
